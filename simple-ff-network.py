
from keras.models import Sequential
from keras.layers import Dense, Activation

import random
from math import *
from pylab import *
import numpy as np

def create_data(n, k, std = 0.1):
	a = 0.5
	b = 0.6
	c = 0
	rx = []
	ry = []
	for i in linspace(0, 2 * pi, k, endpoint=False):
		r = np.random.randn(n)
		sx = np.random.normal(0, a * std, n)
		sy = np.random.normal(0, a * std, n)
		x1 = a * exp(b * r) * cos(r + i) + sx
		x2 = a * exp(b * r) * sin(r + i) + sy
		yy = [0] * k
		yy[c] = 1
		rx += zip(x1, x2);
		ry += [yy] * n
		c+=1

	return (rx, ry)

def normalize(x):
	m = tuple(map(lambda y: mean(y), zip(*x)))
	s = tuple(map(lambda y: std(y), zip(*x)))

	x = np.subtract(x, m)
	x = np.divide(x, s)

	return (x, m, s)

n_class = 5
n_dims = 2

x_train, y_train = create_data(5000, n_class)
x_test, y_test = create_data(200, n_class)

x_train, m, s = normalize(x_train)

x_test = np.subtract(x_test, m)
x_test = np.divide(x_test, s)

model = Sequential()

model.add(Dense(output_dim=64, input_dim=n_dims))
model.add(Activation("relu"))
model.add(Dense(output_dim=64))
model.add(Activation("relu"))
model.add(Dense(output_dim=64))
model.add(Activation("relu"))
model.add(Dense(output_dim=64))
model.add(Activation("relu"))
model.add(Dense(output_dim=n_class))
model.add(Activation("softmax"))

model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

model.fit(x_train, y_train, nb_epoch=10, batch_size=32)
classes = model.predict_classes(x_test, batch_size=32)

xyy = zip(x_test, map(lambda y: argmax(y), y_test), classes)
for i in range(0, n_class):
	x = [x for x,y1,y2 in xyy if y1 == y2 and y2 == i]
	if len(x) > 0:
		x1, x2 = zip(*x)
		plot(x1, x2, ".")

x = [x for x,y1,y2 in xyy if y1 != y2]
if len(x) > 0:
	x1, x2 = zip(*x)
	plot(x1, x2, "c*")

show()
