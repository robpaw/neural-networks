
from optparse import OptionParser

from keras.preprocessing import sequence
from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM, GRU, Activation, Dropout, Embedding
from keras.datasets import imdb

parser = OptionParser()
parser.add_option('-r', '--read', dest='ifile', help='read the model from file')
parser.add_option('-w', '--write', dest='ofile', help='write the model from file')
parser.add_option('-t', '--train', dest='train', help='continue training of the model from file', default=False, action='store_true')
parser.add_option('-e', '--epoch', dest='epochs', help='training epochs', default=1, type='int')

(options, args) = parser.parse_args()

max_features = 20000
max_length = 80
batch_size = 32

print('Loading data...')
(x_train, y_train), (x_test, y_test) = imdb.load_data(path='imdb_full.pkl', nb_words=max_features, seed=113)

print('Preprocessing...')
x_train = sequence.pad_sequences(x_train, maxlen=max_length)
x_test = sequence.pad_sequences(x_test, maxlen=max_length)

model = None
if options.ifile:
	print('Loading the model from ', options.ifile, '...')
	model = load_model(options.ifile)
else:
	print('Constructing the model...')
	model = Sequential()
	model.add(Embedding(max_features, 128, dropout=0.2))
	model.add(GRU(256, return_sequences=False))
	model.add(Dense(1))
	model.add(Activation('sigmoid'))
	model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

if not options.ifile or (options.ifile and options.train):
	print('Training...')
	model.fit(x_train, y_train, batch_size=batch_size, nb_epoch=options.epochs, validation_data=(x_test, y_test), verbose=1)

if options.ofile:
	print('Saving the model under ', options.ofile, '...')
	model.save(options.ofile)

print('Evaluating...')
score, acc = model.evaluate(x_test, y_test, batch_size=batch_size)

print('Test score:', score)
print('Test accuracy:', acc)
